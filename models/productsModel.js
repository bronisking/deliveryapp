const mongoose = require("mongoose");

const products = mongoose.Schema({
  productId: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  quantity: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  measuredIn: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Products", products);
