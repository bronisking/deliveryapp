const mongoose = require("mongoose");

const routes = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Route", routes);
