const jwt = require("jsonwebtoken");

module.exports = function (req, res, next) {
  const token = req.cookies.jwt;

  if (!token)
    return res.send({ message: "Token Not found", loggedStatus: false });

  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
  } catch (error) {
    res.send({ message: "Invalid Token", loggedStatus: false });
  }
  next();
};
