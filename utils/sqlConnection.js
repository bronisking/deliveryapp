const mysql = require("mysql");

let connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "Root123$",
  database: "deliveryApp",
  multipleStatements: true,
});

connection.connect((err) => {
  if (err) {
    console.log(err);
  }
  console.log("Mysql connection successful");
});

module.exports = connection;
