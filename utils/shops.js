const router = require("express").Router();
const { nanoid } = require("nanoid");

const sqlQueryCrud = require("./sqlQueryCrud");
const verifyToken = require("./verifyToken");

router.get(
  "/allshops",
  verifyToken,
  (req, res, next) => {
    req.query =
      "SELECT shop_id,route_id,shop_name,shop_address,shop_phn_number,shop_email,total_credit,longitude, latitude FROM shop WHERE status='true'";
    next();
  },
  sqlQueryCrud()
);

router.post(
  "/addShop",
  verifyToken,
  (req, res, next) => {
    req.query = `INSERT INTO shop (shop_id, route_id, shop_name, shop_address,longitude,latitude, shop_phn_number, shop_email, total_credit, status) VALUES('${nanoid(
      10
    )}','${req.body.route_id}', '${req.body.shop_name}','${
      req.body.shop_address
    }','${req.body.longitude}','${req.body.latitude}','${
      req.body.shop_phn_number
    }','${req.body.shop_email}','0','true')`;
    next();
  },
  sqlQueryCrud()
);

router.post(
  "/deleteShop",
  verifyToken,
  (req, res, next) => {
    req.query = `UPDATE shop set status='false' WHERE shop_id='${req.body.shop_id}'`;
    next();
  },
  sqlQueryCrud()
);

router.post(
  "/updateShopLocation",
  verifyToken,
  (req, res, next) => {
    req.query = `UPDATE SHOP SET longitude='${req.body.longitude}',latitude='${req.body.latitude}' WHERE shop_id='${req.body.shop_id}'`;
    next();
  },
  sqlQueryCrud()
);

module.exports = router;
