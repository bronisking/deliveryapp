const router = require("express").Router();
const User = require("../models/userModel");
const bcrypt = require("bcrypt");
const verifyToken = require("../utils/verifyToken");

router.post("/resetPassword/:username", verifyToken, async (req, res) => {
  const user = await User.findOne({ username: req.params.username });

  if (req.body.newPwd !== req.body.confirmPwd)
    return res.send({
      success: false,
      message: "New password and confirmed new password do not match",
    });

  const validatePwd = await bcrypt.compare(req.body.oldPwd, user.password);

  if (!validatePwd)
    return res.send({ success: false, message: "Invalid current password" });

  const salt = await bcrypt.genSalt(10);

  const hashedPassword = await bcrypt.hash(req.body.newPwd, salt);

  try {
    const result = await User.updateOne(
      { username: req.params.username },
      {
        $set: {
          password: hashedPassword,
          initial: req.body.newPwd,
        },
      }
    );

    res.send({
      success: true,
      message: "Password Changed Successfully",
      result,
    });
  } catch (error) {
    res.send({
      success: false,
      msg: `${error.message}`,
    });
    console.log(error);
  }
});

module.exports = router;
