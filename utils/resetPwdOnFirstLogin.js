const router = require("express").Router();
const User = require("../models/userModel");
const bcrypt = require("bcrypt");
const verifyToken = require("../utils/verifyToken");

router.post("/resetPassword", verifyToken, async (req, res) => {
  const user = await User.findOne({ username: req.body.username });

  if (req.body.newPwd !== req.body.confirmPwd)
    return res.send({
      success: false,
      msg: "New password and confirmed new password do not match",
    });

  const validatePwd = await bcrypt.compare(req.body.oldPwd, user.password);

  if (!validatePwd)
    return res.send({ success: false, message: "Invalid current password" });

  const salt = await bcrypt.genSalt(10);

  const hashedPassword = await bcrypt.hash(req.body.newPwd, salt);

  try {
    const result = await User.updateOne(
      { username: req.body.username },
      {
        $set: {
          password: hashedPassword,
          loginCount: user.loginCount + 1,
          initial: req.body.newPwd,
        },
      }
    );
    res.clearCookie("jwt");

    res.send({
      success: true,
    });
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
