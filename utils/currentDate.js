function currentDate() {
  let date = new Date();

  let todayDate =
    date.getFullYear() +
    "/" +
    `${
      (date.getMonth() + 1).toString().length === 1
        ? "0" + (date.getMonth() + 1)
        : date.getMonth()
    }` +
    "/" +
    date.getDate();
  return todayDate;
}

module.exports = currentDate;
