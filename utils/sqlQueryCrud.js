const connection = require("./sqlConnection");

module.exports = () => {
  return (req, res, next) => {
    let query = req.query;
    connection.query(query, (err, results) => {
      console.log(results, "filter");
      if (err) console.log(err);
      res.send({ results, success: true });
    });
  };
};
