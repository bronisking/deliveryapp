const router = require("express").Router();
const sqlQueryCrud = require("../../utils/sqlQueryCrud");
const currentDate = require("../../utils/currentDate");
const verifyToken = require("../../utils/verifyToken");

router.get(
  "/dashChartData",
  verifyToken,
  (req, res, next) => {
    req.query = `SELECT SUM(o.order_amount) 'total_sales', delivery_date
    from shop_order o
    INNER JOIN order_item o_i ON o.order_id=o_i.order_id
    INNER JOIN delivery_products d_p ON o_i.product_id=d_p.delivery_products_id
    INNER JOIN delivery_info d_i ON d_p.delivery_info_id=d_i.delivery_info_id
    GROUP BY delivery_date
    HAVING YEARWEEK(${`delivery_date`}, 0) = YEARWEEK(CURDATE(), 0)
    order by delivery_date;`;
    next();
  },
  sqlQueryCrud()
);

router.get(
  "/dashTopSalesTable/:filterDate",
  verifyToken,
  (req, res, next) => {
    let filterDate = req.params.filterDate;

    let filterQuery = ``;

    if (filterDate === "daily")
      filterQuery = `d_i.delivery_date='${currentDate()}'`;
    if (filterDate === "weekly")
      filterQuery = `YEARWEEK(${`delivery_date`}, 0) = YEARWEEK(CURDATE(), 1)`;
    if (filterDate === "monthly")
      filterQuery = `MONTH(d_i.delivery_date) = MONTH(CURRENT_DATE())`;
    if (filterDate === "yearly")
      filterQuery = `YEAR(d_i.delivery_date) = YEAR(CURRENT_DATE())`;

    req.query = `SELECT 
    SUM(o.order_amount) 'totalSales',s.shop_name
    FROM route d_r
    INNER JOIN shop s ON d_r.route_id=s.route_id
    INNER JOIN shop_order o ON s.shop_id=o.shop_id
    INNER JOIN order_item o_i ON o.order_id=o_i.order_id
    INNER JOIN delivery_products d_p_s ON d_p_s.delivery_products_id=o_i.product_id
    INNER JOIN product p on d_p_s.product_id=p.product_id
    INNER JOIN delivery_info d_i ON d_i.delivery_info_id=d_p_s.delivery_info_id
    WHERE ${filterQuery}
    group by s.shop_id, shop_name
    order by totalSales desc
    LIMIT 5`;
    next();
  },
  sqlQueryCrud()
);

router.get(
  "/dashCard",
  verifyToken,
  (req, res, next) => {
    req.query = `
      SELECT SUM(o.order_amount) as 'totalSales'
      from shop_order o
      INNER JOIN order_item o_i ON o.order_id=o_i.order_id
      INNER JOIN delivery_products d_p ON o_i.product_id=d_p.delivery_products_id
      INNER JOIN delivery_info d_i ON d_p.delivery_info_id=d_i.delivery_info_id
      WHERE d_i.delivery_date='${currentDate()}';
      select SUM(ci.credit_amount) as totalCredit
      FROM shop_order
      INNER JOIN credit_info ci on shop_order.order_id = ci.order_id
      LEFT JOIN credit_info c on shop_order.order_id = c.order_id
      LEFT JOIN shop s on shop_order.shop_id = s.shop_id
      LEFT JOIN route dr on s.route_id = dr.route_id
      LEFT JOIN delivery_info di on dr.route_id = di.route_id
      WHERE delivery_date='${currentDate()}';
      SELECT SUM(received_amount) as creditReceived
      FROM delivery_info di
      INNER JOIN shop s on di.route_id = s.route_id
      INNER JOIN credit_received cr on s.shop_id = cr.shop_id
      WHERE delivery_date='${currentDate()}';
      select shop_name, total_credit 
      from shop 
      WHERE total_credit=(select MAX(total_credit) from shop);
  `;
    next();
  },
  sqlQueryCrud()
);

module.exports = router;
