const router = require("express").Router();
const genPwd = require("generate-password");
const bcrypt = require("bcrypt");
const User = require("../../models/userModel");
const emailResetPwd = require("./emailResetPwd");
const verifyToken = require("../../utils/verifyToken");

router.post("/resetPwd", verifyToken, async (req, res) => {
  const findUser = await User.findOne({ username: req.body.username });

  const password = genPwd.generate({
    length: 6,
    numbers: true,
  });

  const salt = await bcrypt.genSalt(10);

  const hashedPwd = await bcrypt.hash(password, salt);

  if (!findUser) return res.send({ error: true, message: "User do not exist" });

  try {
    const result = await User.updateOne(
      { username: req.body.username },
      {
        $set: {
          password: hashedPwd,
          initial: password,
          loginCount: 0,
        },
      }
    );
    console.log(req.body);
    emailResetPwd(password, findUser.email, findUser.name);
    res.send({
      success: true,
      message: `Reset Password for username ${findUser.name} was successful`,
    });
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
