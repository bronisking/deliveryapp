const router = require("express").Router();
const { nanoid } = require("nanoid");
const sqlQueryCrud = require("../../utils/sqlQueryCrud");
const verifyToken = require("../../utils/verifyToken");

router.get(
  "/productList",
  verifyToken,
  (req, res, next) => {
    req.query = "SELECT * FROM product WHERE status='true'";
    next();
  },
  sqlQueryCrud()
);

router.post(
  "/addProduct",
  verifyToken,
  async (req, res, next) => {
    let product_id = `bdc-${req.body.product_name.charAt(0) + nanoid(5)}`;
    req.query = `INSERT INTO product (product_id, product_name, product_rate, product_quantity, measured_in,status) VALUES(${req.body.product_id},'${req.body.product_name}','${req.body.product_rate}','${req.body.product_quantity}', '${req.body.measured_in}','true')`;
    next();
  },
  sqlQueryCrud()
);

router.put(
  "/updateProduct/:productId",
  verifyToken,
  async (req, res, next) => {
    req.query = `UPDATE product SET product_name='${req.body.product_name}', product_rate='${req.body.product_rate}', product_quantity='${req.body.product_quantity}', measured_in='${req.body.measured_in}' WHERE (product_id='${req.params.productId}')`;
    next();
  },
  sqlQueryCrud()
);

router.post(
  "/deleteProduct",
  verifyToken,
  (req, res, next) => {
    req.query = `UPDATE product SET status='false' WHERE product_id='${req.body.product_id}'`;
    next();
  },
  sqlQueryCrud()
);

module.exports = router;
