const nodemailer = require("nodemailer");

async function sendUserCreatedMail(username, password, email, name) {
  const output = `
  <style>@media only screen and (max-width: 620px){table[class=body] h1{font-size:28px !important;margin-bottom:10px !important}table[class=body] p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a{font-size:16px !important}table[class=body] .wrapper, table[class=body] .article{padding:10px !important}table[class=body] .content{padding:0 !important}table[class=body] .container{padding:0 !important;width:100% !important}table[class=body] .main{border-left-width:0 !important;border-radius:0 !important;border-right-width:0 !important}table[class=body] .btn table{width:100% !important}table[class=body] .btn a{width:100% !important}table[class=body] .img-responsive{height:auto !important;max-width:100% !important;width:auto !important}}</style><body style="background-color: white; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; width:70%; mso-table-rspace: 0pt; width: 100%; background-color: #333a56; padding:10px"><tr><td style="vertical-align: top; text-align:center; margin:10px auto 10px auto"><img src="https://bdcfoods.com/static/images/deliveredloginpage.png" style="filter: drop-shadow(0 0 2px #000);" /></td></tr><tr><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;"><div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;"><table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;"><tr><td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"><table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;"><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><h3>Dear ${name},</h3><h4>Delivered Cargo account created</h4></p><p style="font-weight: normal; margin: 0; Margin-bottom: 15px;"> Your delivered account has been created, proceed to login with following credentials.</p><table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;"><tbody><tr><td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;"><table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;"><tbody><tr><ul><li><h4>Username: ${username}</h2></li><li><h4>Password: ${password}</h4></li></ul></tr></tbody></table></td></tr></tbody></table><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"> Head over to <a href="http://localhost:3000/">Delivered Cargo</a></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"> Good luck! For your work.</p></td></tr></table></td></tr></table><div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;"><table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;"><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="color:white">© 2021 Delivered Cargo (P) LTD. All Rights Reserved.</p></td></tr></table></div></div></td></tr></table></body>
  `;
  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "mail.bdcfoods.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: process.env.E_USER,
      pass: process.env.E_PWD,
    },
    tls: {
      rejectUnauthorized: false,
    },
  });

  // send mail with defined transport object
  try {
    let info = await transporter.sendMail({
      from: `${name} <contact@bdcfoods.com>`, // sender address
      to: `${email}`, // list of receivers
      subject: "Delivered Cargo user created", // Subject line
      text: "Delivered Cargo", // plain text body
      html: output, // html body
    });
    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  } catch (error) {
    console.log(error);

    // messages = "n";
  }
}

module.exports = sendUserCreatedMail;
