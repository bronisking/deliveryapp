const router = require("express").Router();
const sqlQueryCrud = require("../../utils/sqlQueryCrud");
const verifyToken = require("../../utils/verifyToken");

router.get(
  "/getRoutes",
  verifyToken,
  (req, res, next) => {
    req.query = "SELECT * FROM route";
    next();
  },
  sqlQueryCrud()
);

module.exports = router;
