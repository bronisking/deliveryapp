const router = require("express").Router();
const User = require("../../models/userModel");
const genPwd = require("generate-password");
const bcrypt = require("bcrypt");
const sendUserCreatedMail = require("./sendMail");
const verifyToken = require("../../utils/verifyToken");

router.post("/createUser", async (req, res) => {
  // let userCounter = 1;

  // let userCount = 0;

  // checking user count
  await User.countDocuments({}, (err, count) => {
    userCount = count;
  });

  // get all the users from db
  const countDoc = await User.find().sort({ createdAt: -1 });

  // check if user count is less than 1
  // formatting username to get last user count of all created
  let userCounter =
    userCount === 0 ? 1 : Number(countDoc[0].username.match(/\d+/)[0]) + 1;

  console.log(userCounter);
  // console.log(countDoc[0].username.match(/\d+/)[0]) + 1;

  // generating random pwd
  const password = genPwd.generate({
    length: 6,
    numbers: true,
  });

  // hashing the pwd
  const salt = await bcrypt.genSalt(10);
  const hashedPwd = await bcrypt.hash(password, salt);

  // creating unique username
  const username = `bdc-${req.body.name.split(" ")[0]}${req.body.role.charAt(
    0
  )}${userCounter}`.toLowerCase();

  // creating the new user
  let user = new User({
    name: req.body.name,
    role: req.body.role.toLowerCase(),
    username: username,
    password: hashedPwd,
    email: req.body.email,
    initial: password,
    loginCount: 0,
  });

  // saving the created user

  try {
    user = await user.save();
    sendUserCreatedMail(username, password, req.body.email, req.body.name);
    console.log(userCounter);
    res.send({ success: true, user: username });
  } catch (error) {
    res.send({ error: `Some error try again later ${error}`, success: false });
  }
  // userCounter++;
});

module.exports = router;
