const router = require("express").Router();
const User = require("../../models/userModel");
const verifyToken = require("../../utils/verifyToken");

router.get("/fetchUsers", verifyToken, async (req, res) => {
  const users = await User.find().select("-password").sort({ createdAt: -1 });
  res.send(users);
});

module.exports = router;
