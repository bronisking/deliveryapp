const router = require("express").Router();
const User = require("../../models/userModel");
const verifyToken = require("../../utils/verifyToken");

router.post("/searchUser", verifyToken, async (req, res) => {
  try {
    const users = await User.find({
      name: { $regex: `${req.body.query}`, $options: "$i" },
    });
    res.send({
      success: true,
      users,
    });
    console.log(users);
  } catch (e) {
    console.log(e);
  }
});

module.exports = router;
