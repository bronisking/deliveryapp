const router = require("express").Router();
const User = require("../../models/userModel");
const verifyToken = require("../../utils/verifyToken");

router.delete("/deleteUser", verifyToken, async (req, res) => {
  console.log(req.body.id);
  const id = await User.findByIdAndDelete(req.body.id);
  res.send({ msg: "User Deleted", id });
});

module.exports = router;
