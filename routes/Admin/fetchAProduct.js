const router = require("express").Router();
const sqlQueryCrud = require("../../utils/sqlQueryCrud");
const verifyToken = require("../../utils/verifyToken");

router.get(
  "/fetchProduct/:productId",
  verifyToken,
  (req, res, next) => {
    req.query = `SELECT * FROM product WHERE product_id='${req.params.productId}'`;
    next();
  },
  sqlQueryCrud()
);

module.exports = router;
