const router = require("express").Router();
const DeliveryRoutes = require("../../models/routeModel");
const verifyToken = require("../../utils/verifyToken");

router.get("/deliveryRoutes", verifyToken, async (req, res) => {
  const deliveryRoutes = await DeliveryRoutes.find();
  res.send({ routes: deliveryRoutes });
});

router.post("/deliveryRoutes", async (req, res) => {
  let deliveryRoutes = new DeliveryRoutes({
    name: req.body.name,
  });

  try {
    deliveryRoutes = await deliveryRoutes.save();

    res.send(deliveryRoutes);
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
