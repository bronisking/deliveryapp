const router = require("express").Router();
const currentDate = require("../../utils/currentDate");
const sqlQueryCrud = require("../../utils/sqlQueryCrud");
const verifyToken = require("../../utils/verifyToken");

router.get(
  "/todaysData/:shop_id/:route_id",
  verifyToken,
  (req, res, next) => {
    let filterQuery = `${
      req.params.shop_id === "undefined" || req.params.shop_id === "all"
        ? ""
        : "AND s.shop_id=" + "'" + req.params.shop_id + "'"
    } ${
      req.params.route_id === "all" || req.params.route_id === undefined
        ? ""
        : "AND d_r.route_id=" + "'" + req.params.route_id + "'"
    }`;

    req.query = `SELECT 
    s.shop_name,
    o.order_amount,
    s.shop_id, o.order_id
    FROM route d_r
    INNER JOIN shop s ON d_r.route_id=s.route_id
    INNER JOIN shop_order o ON s.shop_id=o.shop_id
    INNER JOIN order_item o_i ON o.order_id=o_i.order_id
    INNER JOIN delivery_products d_p_s ON d_p_s.delivery_products_id=o_i.product_id
    INNER JOIN product p on d_p_s.product_id=p.product_id
    INNER JOIN delivery_info d_i ON d_i.delivery_info_id=d_p_s.delivery_info_id
    WHERE d_i.delivery_date='${currentDate()}' ${filterQuery} 
    GROUP BY o.order_id
    ORDER BY o.order_amount desc;
    SELECT 
    SUM(o.order_amount) 'totalSales'
    FROM route d_r
    INNER JOIN shop s ON d_r.route_id=s.route_id
    INNER JOIN shop_order o ON s.shop_id=o.shop_id
    INNER JOIN order_item o_i ON o.order_id=o_i.order_id
    INNER JOIN delivery_products d_p_s ON d_p_s.delivery_products_id=o_i.product_id
    INNER JOIN product p on d_p_s.product_id=p.product_id
    INNER JOIN delivery_info d_i ON d_i.delivery_info_id=d_p_s.delivery_info_id
    WHERE d_i.delivery_date='${currentDate()}' ${filterQuery} `;
    next();
  },
  sqlQueryCrud()
);

router.get(
  "/detailView/:order_id",
  verifyToken,
  (req, res, next) => {
    req.query = `SELECT 
    s.shop_name,
    o.order_amount,
    p.product_name,
    o_i.line_total, 
    o_i.quantity
    from
    shop s
    INNER JOIN shop_order o on s.shop_id=o.shop_id
    INNER JOIN order_item o_i on o.order_id=o_i.order_id
    INNER JOIN delivery_products d_p_s ON d_p_s.delivery_products_id=o_i.product_id
    INNER JOIN product p on d_p_s.product_id=p.product_id
    WHERE o.order_id='${req.params.order_id}' `;
    console.log(req.params.order_id);
    next();
  },
  sqlQueryCrud()
);

module.exports = router;
