const router = require("express").Router();
const currentDate = require("../../utils/currentDate");
const sqlQueryCrud = require("../../utils/sqlQueryCrud");
const verifyToken = require("../../utils/verifyToken");

router.get(
  "/allData/:shop_id/:route_id/:date/:salesman/:page",
  verifyToken,
  (req, res, next) => {
    let filterQuery = `${
      req.params.shop_id === undefined || req.params.shop_id === "all"
        ? ""
        : "AND s.shop_id=" + "'" + req.params.shop_id + "'"
    } ${
      req.params.route_id === "all" || req.params.route_id === undefined
        ? ""
        : "AND d_r.route_id=" + "'" + req.params.route_id + "'"
    } ${
      req.params.date === "all" || req.params.date === undefined
        ? ""
        : "AND d_i.delivery_date=" + "'" + req.params.date + "'"
    } ${
      req.params.salesman === "all" || req.params.salesman === undefined
        ? ""
        : "AND d_i.delivery_salesman=" + "'" + req.params.salesman + "'"
    }`;

    let offset =
      parseInt(req.params.page) === 1 ? 0 : (req.params.page - 1) * 10;

    req.query = `SELECT 
    s.shop_name,
    o.order_amount,
    s.shop_id, 
    o.order_id,
    d_i.delivery_date
    FROM route d_r
    INNER JOIN shop s ON d_r.route_id=s.route_id
    INNER JOIN delivery_info d_i ON d_i.route_id=d_r.route_id
    INNER JOIN shop_order o ON s.shop_id=o.shop_id WHERE 1=1 ${filterQuery}
    Group by s.shop_name,
    o.order_amount,
    s.shop_id, 
    o.order_id
    ORDER BY d_i.delivery_date desc
    LIMIT 10 OFFSET ${offset};
    SELECT 
    SUM(o.order_amount) 'totalSales'
    FROM route d_r
    INNER JOIN shop s ON d_r.route_id=s.route_id
    INNER JOIN shop_order o ON s.shop_id=o.shop_id
    INNER JOIN order_item o_i ON o.order_id=o_i.order_id
    INNER JOIN delivery_products d_p_s ON d_p_s.delivery_products_id=o_i.product_id
    INNER JOIN product p on d_p_s.product_id=p.product_id
    INNER JOIN delivery_info d_i ON d_i.delivery_info_id=d_p_s.delivery_info_id
    WHERE 1=1 ${filterQuery}
    `;
    next();
  },
  sqlQueryCrud()
);

module.exports = router;
