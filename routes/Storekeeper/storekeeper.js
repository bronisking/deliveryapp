const router = require("express").Router();
const { nanoid } = require("nanoid");
const sqlQueryCrud = require("../../utils/sqlQueryCrud");
const verifyToken = require("../../utils/verifyToken");

router.post(
  "/deliverySlip",
  verifyToken,
  (req, res, next) => {
    const delivery_info_id = nanoid(10);

    let products = req.body.products;

    let insert_products = `INSERT INTO delivery_products (delivery_products_id,product_id, quantity, delivery_info_id) VALUES ${products.map(
      (product) =>
        "(" +
        "'" +
        nanoid(10) +
        "'" +
        "," +
        "'" +
        product.product_id +
        "'" +
        "," +
        "'" +
        product.amount +
        "'" +
        "," +
        "'" +
        delivery_info_id +
        "'" +
        ")"
    )}`;

    let date = new Date();

    let todayDate =
      date.getFullYear() +
      "/" +
      `${
        (date.getMonth() + 1).toString().length === 1
          ? "0" + (date.getMonth() + 1)
          : date.getMonth()
      }` +
      "/" +
      date.getDate();

    console.log(todayDate);

    req.query = `INSERT INTO delivery_info (delivery_date, delivery_driver,delivery_salesman,route_id,delivery_vehicle) VALUES ( '${todayDate}', '${req.body.delivery_driver}','${req.body.delivery_salesman}','${req.body.route_id}','${req.body.delivery_vehicle}'); ${insert_products}`;

    next();
  },
  sqlQueryCrud()
);

module.exports = router;
