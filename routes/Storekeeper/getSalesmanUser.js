const router = require("express").Router();
const User = require("../../models/userModel");
const verifyToken = require("../../utils/verifyToken");

router.get("/getSalesmanUser", verifyToken, async (req, res) => {
  try {
    let salesmanUsers = await User.find(
      { role: "salesman" },
      { name: true, username: true }
    );
    res.send({ salesman: salesmanUsers, success: true });
  } catch (error) {
    res.send({ success: false, error: error });
  }
});

module.exports = router;
