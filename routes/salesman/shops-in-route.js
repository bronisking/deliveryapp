const router = require("express").Router();

const sqlQueryCrud = require("../../utils/sqlQueryCrud");
const verifyToken = require("../../utils/verifyToken");

router.get(
  "/shop-in-route/:salesman",
  verifyToken,
  (req, res, next) => {
    req.query = `SELECT shop_id, shop_address, shop_name,total_credit  
                  from shop
                INNER JOIN route dr on shop.route_id = dr.route_id;`
                // INNER JOIN delivery_info di on dr.route_id = di.route_id
                // WHERE di.delivery_salesman='${req.params.salesman}'
                // GROUP BY shop_id, 2,3,4;`;
                // WHERE di.delivery_salesman='${req.params.salesman}'
    // console.log(req.query);
    next();
  },
  sqlQueryCrud()
);

module.exports = router;
