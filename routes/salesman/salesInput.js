const router = require("express").Router();
const { nanoid } = require("nanoid");
const sqlQueryCrud = require("../../utils/sqlQueryCrud");
const verifyToken = require("../../utils/verifyToken");

router.post(
  "/inputSalesData",
  verifyToken,
  (req, res, next) => {
    const order_id = nanoid(10);

    let products = req.body.products;

    let insert_products = `INSERT INTO ORDER_ITEM (order_item_id,order_id, product_id,quantity,line_total) VALUES ${products.map(
      (product) =>
        "(" +
        "'" +
        nanoid(10) +
        "'" +
        "," +
        "'" +
        order_id +
        "'" +
        "," +
        "'" +
        product.product_id +
        "'" +
        "," +
        "'" +
        product.amount +
        "'" +
        "," +
        "'" +
        product.line_total +
        "'" +
        ")"
    )}`;

    let credit_info = `INSERT INTO CREDIT_INFO (credit_info_id,credit_amount,order_id) VALUES('${nanoid(
      10
    )}','${req.body.creditAmount}','${order_id}')`;

    let credit_received = `INSERT INTO CREDIT_RECEIVED (credit_received_id,received_amount,shop_id) VALUES('${nanoid()}','${
      req.body.creditReceived
    }','${req.body.shop}')`;

    let updateCreditQuery = `UPDATE shop SET total_credit=${req.body.totalCreditAmount} WHERE shop_id='${req.body.shop}'`;

    req.query = `INSERT INTO SHOP_ORDER (order_id,shop_id,order_amount) VALUES ('${order_id}','${
      req.body.shop
    }','${req.body.totalAmount}'); ${insert_products}; ${updateCreditQuery}; ${
      req.body.creditAmount !== 0 ? credit_info + ";" : ""
    } ${req.body.creditReceived !== 0 ? credit_received + ";" : ""}`;

    console.log(req.query);
    next();
  },
  sqlQueryCrud()
);

router.put(
  "/updateCredit",
  verifyToken,
  (req, res, next) => {
    req.query = `INSERT INTO CREDIT_RECEIVED (credit_received_id,received_amount,shop_id) VALUES('${nanoid()}','${
      req.body.creditReceived
    }','${req.body.shop}'); UPDATE shop set total_credit='${
      req.body.totalCreditAmount
    }' WHERE shop_id='${req.body.shop}'`;

    console.log(req.body.totalCreditAmount);
    console.log(req.body.shop);
    next();
  },
  sqlQueryCrud()
);

module.exports = router;
