const router = require("express").Router();
const sqlQueryCrud = require("../../utils/sqlQueryCrud");
const verifyToken = require("../../utils/verifyToken");

router.get(
  "/getSlipProducts/:user",
  verifyToken,
  (req, res, next) => {
    let date = new Date();

    let todayDate =
      date.getFullYear() +
      "/" +
      `${
        (date.getMonth() + 1).toString().length === 1
          ? "0" + (date.getMonth() + 1)
          : date.getMonth()
      }` +
      "/" +
      date.getDate();

    req.query = `SELECT p.product_name, d_p.delivery_products_id as product_id, p.product_rate
    FROM product p 
    INNER JOIN delivery_products d_p ON p.product_id=d_p.product_id 
    INNER JOIN delivery_info d_i ON d_p.delivery_info_id=d_i.delivery_info_id
    WHERE d_i.delivery_salesman='${req.params.user}'`;
    // console.log(req.query);
    next();
  },
  sqlQueryCrud()
);

module.exports = router;
