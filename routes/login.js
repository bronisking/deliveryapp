const router = require("express").Router();
const jwt = require("jsonwebtoken");
const User = require("../models/userModel");
const bcrypt = require("bcrypt");

router.post("/login", async (req, res) => {
  const user = await User.findOne({ username: req.body.username });
  if (!user) return res.send({ success: false, msg: "User do not exist" });

  const validPass = await bcrypt.compare(req.body.password, user.password);

  if (!validPass)
    return res.send({ success: false, msg: "User do not exist pwd" });

  if ((await user.loginCount) >= 1) {
    try {
      const result = await User.updateOne(
        { username: req.body.username },
        {
          $set: {
            loginCount: user.loginCount + 1,
          },
        }
      );
    } catch (error) {
      console.log(error);
    }
  }
  const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET, {
    expiresIn: "7h",
  });

  res.cookie("jwt", token, {
    httpOnly: true,
    sameSite: "strict",
    maxAge: 25200000,
  });

  if ((await user.loginCount) === 0) {
    res.send({
      success: true,
      username: user.username,
      resetPwd: true,
    });
  } else {
    res.send({
      success: true,
      name: user.name,
      username: user.username,
      resetPwd: false,
      role: user.role,
      email: user.email,
    });
  }
});

router.get("/loggedJwt", (req, res) => {
  const cookies = req.cookies;
  if (cookies.jwt) {
    res.send({ hasCookie: true });
  } else {
    res.send({ hasCookie: false });
  }
});

router.get("/logout", (req, res) => {
  res.clearCookie("jwt").send({ message: "cleared" });
});

module.exports = router;
